import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Film } from '../models/film';
import { People } from '../models/people';
import { ThrowStmt } from '@angular/compiler';
import { Planet } from '../models/planet';
import { Species } from '../models/species';
import { Vehicle } from '../models/vehicle';
import { Starship } from '../models/starship';

@Injectable({
  providedIn: 'root'
})
export class MainserviceService {
  swApiUrl = 'https://swapi.co/api';
  moviesUrl = '/films';
  peopleUrl = '/people';
  planetsUrl = '/planets';
  speciesUrl = '/species';
  vehiclesUrl = '/vehicles';
  starshipsUrl = '/starships';

  constructor(private http: HttpClient) { }

  getMovies(): Observable<Film[]> {
    return this.http.get<Film[]>(`${this.swApiUrl}${this.moviesUrl}`);
  }
  getMovie(id): Observable<Film>{
    return this.http.get<Film>(`${this.swApiUrl}${this.moviesUrl}/${id}`);
  }
  getPeople(): Observable<People[]>{
    return this.http.get<People[]>(`${this.swApiUrl}${this.peopleUrl}`);
  }
  getPerson(id): Observable<People>{
    return this.http.get<People>(`${this.swApiUrl}${this.peopleUrl}/${id}`);
  }
  getPlanets(): Observable<Planet[]>{
    return this.http.get<Planet[]>(`${this.swApiUrl}${this.planetsUrl}`);
  }
  getPlanet(id): Observable<Planet>{
    return this.http.get<Planet>(`${this.swApiUrl}${this.planetsUrl}/${id}`);
  }
  getSpecies(): Observable<Species[]>{
    return this.http.get<Species[]>(`${this.swApiUrl}${this.speciesUrl}`);
  }
  getSpecie(id): Observable<Species>{
    return this.http.get<Species>(`${this.swApiUrl}${this.speciesUrl}/${id}`);
  }
  getVehicles(): Observable<Vehicle[]>{
    return this.http.get<Vehicle[]>(`${this.swApiUrl}${this.vehiclesUrl}`);
  }
  getVehicle(id): Observable<Vehicle>{
    return this.http.get<Vehicle>(`${this.swApiUrl}${this.vehiclesUrl}/${id}`);
  }
  getStarships(): Observable<Starship[]>{
    return this.http.get<Starship[]>(`${this.swApiUrl}${this.starshipsUrl}`);
  }
  getStarship(id): Observable<Starship>{
    return this.http.get<Starship>(`${this.swApiUrl}${this.starshipsUrl}/${id}`);
  }

}
