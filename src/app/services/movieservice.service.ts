import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Movie } from '../models/movie';
import { Video } from '../models/video';
import { ListOfMovies } from '../models/list-of-movies';
import { ListOfVideos } from '../models/list-of-videos';

@Injectable({
  providedIn: 'root'
})
export class MovieserviceService {

  tmDBImgUrl = "https://image.tmdb.org/t/p/w500";
  tmDBFullImgUrl = "https://image.tmdb.org/t/p/original";
  tmDBApiUrl = 'https://api.themoviedb.org/3';
  api_key = "f6c366f3ca5cd53746ef7cb446d01f05"
  GetUrl = '/movie/';
  SearchUrl = `/search/movie?api_key=${this.api_key}&language=en-US&page=1&include_adult=false`;
  GetVideoUrl = ``;

  constructor(private http: HttpClient) { }

  searchMovie(moviename): Observable<ListOfMovies> {
    return this.http.get<ListOfMovies>(`${this.tmDBApiUrl}${this.SearchUrl}&query=${moviename}`);
  }
  getMovie(movieid): Observable<Movie>{
    return this.http.get<Movie>(`${this.tmDBApiUrl}${this.GetUrl}${movieid}?api_key=${this.api_key}`);
  }
  getVideos(movieid): Observable<ListOfVideos>{
    return this.http.get<ListOfVideos>(`${this.tmDBApiUrl}${this.GetUrl}${movieid}/videos?api_key=${this.api_key}`);
  }
}
