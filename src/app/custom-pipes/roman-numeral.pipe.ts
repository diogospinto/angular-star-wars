import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'romanNumeral'
})
export class RomanNumeralPipe implements PipeTransform {

  transform(value: number, ...args: any[]): string {
    return (value == 1 ? 'I' : value == 2 ? 'II' : value == 3 ? 'III' : value == 4 ? 'IV' : value == 5 ? 'V' : value == 6 ? 'VI' : value == 7 ? 'VII' : value == 8 ? 'VIII' : value == 9 ? 'IX' : 'X');
  }

}