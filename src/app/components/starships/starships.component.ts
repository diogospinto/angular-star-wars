import { Component, OnInit } from '@angular/core';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { Starship } from 'src/app/models/starship';

@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.css']
})
export class StarshipsComponent implements OnInit {

  starships:Starship[];
  res=[];

  constructor(private mainservice:MainserviceService) { }

  ngOnInit() {
    this.mainservice.getStarships().subscribe(starships=>{
      this.res.push(starships);
      this.starships = this.res[0].results;

      this.starships.forEach(starship=>{
        starship.id = parseInt(starship.url.split('/')[5]);
        starship.films_obj = [];
        starship.pilots_obj = [];

        starship.films.forEach(filmUrl=>{
          this.mainservice.getMovie(parseInt(filmUrl.split('/')[5])).subscribe(film=>{
            film.id = parseInt(film.url.split('/')[5]);
            starship.films_obj.push(film);
          });
        });

        starship.pilots.forEach(pilotsUrl=>{
          this.mainservice.getPerson(parseInt(pilotsUrl.split('/')[5])).subscribe(pilot=>{
            pilot.id = parseInt(pilot.url.split('/')[5]);
            starship.pilots_obj.push(pilot);
          });
        });

      });
    });
  }
}
