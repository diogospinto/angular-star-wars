import { Component, OnInit } from '@angular/core';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { Species } from 'src/app/models/species';

@Component({
  selector: 'app-species',
  templateUrl: './species.component.html',
  styleUrls: ['./species.component.css']
})
export class SpeciesComponent implements OnInit {

  species:Species[];
  res = [];

  constructor(private mainservice:MainserviceService) { }

  ngOnInit() {
    this.mainservice.getSpecies().subscribe(species=>{
      this.species = species;
      this.res.push(species);
      this.species = this.res[0].results;
      this.species.forEach(specie=>{
        specie.id = parseInt(specie.url.split('/')[5]);
        specie.people_obj = [];
        specie.films_obj = [];
        specie.films.forEach(filmUrl=>{
          this.mainservice.getMovie(parseInt(filmUrl.split('/')[5])).subscribe(film=>{
            film.id = parseInt(film.url.split('/')[5]);
            specie.films_obj.push(film);
          });
        });
        specie.people.forEach(peopleUrl=>{
          this.mainservice.getPerson(parseInt(peopleUrl.split('/')[5])).subscribe(person=>{
            person.id = parseInt(person.url.split('/')[5]);
            specie.people_obj.push(person);
          });
        });
      });
    });
  }

}
