import { Component, OnInit } from '@angular/core';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { Planet } from 'src/app/models/planet';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {

  res = [];
  planets:Planet[];

  constructor(private mainservice:MainserviceService) { }

  ngOnInit() {
    this.mainservice.getPlanets().subscribe(planets=>{
      this.res.push(planets);
      this.planets = this.res[0].results;

      this.planets.forEach(planet=>{
        planet.id = parseInt(planet.url.split('/')[5]);
        planet.people_obj = [];
        planet.films_obj = [];

        planet.residents.forEach(personUrl => {
          this.mainservice.getPerson(parseInt(personUrl.split('/')[5])).subscribe(person => {
              person.id = parseInt(person.url.split('/')[5]);
              planet.people_obj.push(person);
          });
        });

        planet.films.forEach(filmUrl=>{
          this.mainservice.getMovie(parseInt(filmUrl.split('/')[5])).subscribe(film=>{
            film.id = parseInt(film.url.split('/')[5]);
            planet.films_obj.push(film);
          });
        });
      });
    });
  }

}
