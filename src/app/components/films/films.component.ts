import { Component, OnInit } from '@angular/core';
import { Film } from '../../models/film';
import { MainserviceService } from '../../services/mainservice.service';
import { Movie } from 'src/app/models/movie';
import { Video } from 'src/app/models/video';
import { MovieserviceService } from 'src/app/services/movieservice.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {

  res = [];
  films: Film[];
  movies: Movie[];

  constructor(private mainService: MainserviceService, private movieService:MovieserviceService, private sanitization:DomSanitizer) { }

  ngOnInit() {
    this.mainService.getMovies().subscribe(films => {

      this.films = films;
      this.res.push(films);
      this.films = this.res[0].results;
      this.movies = [];

      this.films.forEach(film=>{
        this.movieService.searchMovie(film.title).subscribe(movie=>{

          this.movies.push(movie.results[0]);

          film.poster_path = this.movieService.tmDBImgUrl+'/'+movie.results[0].poster_path;
          film.backdrop_path = this.movieService.tmDBFullImgUrl+'/'+movie.results[0].backdrop_path;
          film.backdrop_path_safe = this.sanitization.bypassSecurityTrustStyle(`url(${film.backdrop_path})`);
          film.id = parseInt(film.url.split('/')[5]);
        });
      });

      this.films.sort((a, b) => (a.episode_id < b.episode_id) ? 1 : -1);
    });
    
  }

}
