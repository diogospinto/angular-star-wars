import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { Starship } from 'src/app/models/starship';

@Component({
  selector: 'app-starship',
  templateUrl: './starship.component.html',
  styleUrls: ['./starship.component.css']
})
export class StarshipComponent implements OnInit {

  id:number;
  starship:Starship;

  constructor(private route:ActivatedRoute, private mainservice:MainserviceService) { }

  ngOnInit() {
    this.id=parseInt(this.route.snapshot.paramMap.get('id'));
    this.mainservice.getStarship(this.id).subscribe(starship=>{
      this.starship = starship;
      this.starship.id = parseInt(this.starship.url.split('/')[5]);

      console.log(this.starship);

      this.starship.films_obj = [];
      this.starship.pilots_obj = [];
      this.starship.films.forEach(filmUrl=>{
        this.mainservice.getMovie(parseInt(filmUrl.split('/')[5])).subscribe(film=>{
          film.id = parseInt(film.url.split('/')[5]);
          this.starship.films_obj.push(film);
        });
      });
      this.starship.pilots.forEach(pilotsUrl=>{
        this.mainservice.getPerson(parseInt(pilotsUrl.split('/')[5])).subscribe(pilot=>{
          pilot.id = parseInt(pilot.url.split('/')[5]);
          this.starship.pilots_obj.push(pilot);
        });
      });
    });
  }

}
