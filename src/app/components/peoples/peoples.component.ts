import { Component, OnInit } from '@angular/core';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { People } from 'src/app/models/people';
import { Movie } from 'src/app/models/movie';

@Component({
  selector: 'app-peoples',
  templateUrl: './peoples.component.html',
  styleUrls: ['./peoples.component.css']
})
export class PeoplesComponent implements OnInit {

  res = [];
  people: People[];
  movies: Movie[];

  constructor(private mainservice: MainserviceService) { }

  ngOnInit() {
    this.mainservice.getPeople().subscribe(people=>{

      this.people = people;
      this.res.push(people);
      this.people = this.res[0].results;

      this.people.forEach(person=>{
        person.id = parseInt(person.url.split('/')[5]);
        person.films_obj = [];
        person.films.forEach(filmlink=>{
          this.mainservice.getMovie(filmlink.split('/')[5]).subscribe(film=>{
            film.id = parseInt(film.url.split('/')[5]);
            person.films_obj.push(film);
          });
        });

        person.films_obj.sort((a, b) => (a.episode_id > b.episode_id) ? 1 : -1);
        
      });
    });
  }

}
