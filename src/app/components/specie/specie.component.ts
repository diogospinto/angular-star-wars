import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { Species } from 'src/app/models/species';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-specie',
  templateUrl: './specie.component.html',
  styleUrls: ['./specie.component.css']
})
export class SpecieComponent implements OnInit {

  id:number;
  specie:Species;

  constructor(private route:ActivatedRoute, private mainservice:MainserviceService) { }

  ngOnInit() {
    this.id=parseInt(this.route.snapshot.paramMap.get('id'));
    this.mainservice.getSpecie(this.id).subscribe(specie=>{
      this.specie = specie;
      this.specie.id = parseInt(this.specie.url.split('/')[5]);
      
      this.specie.films_obj = [];
      this.specie.people_obj = [];
      this.specie.films.forEach(filmUrl=>{
        this.mainservice.getMovie(parseInt(filmUrl.split('/')[5])).subscribe(film=>{
          film.id = parseInt(film.url.split('/')[5]);
          this.specie.films_obj.push(film);
        });
      });
      this.specie.people.forEach(peopleUrl=>{
        this.mainservice.getPerson(parseInt(peopleUrl.split('/')[5])).subscribe(person=>{
          person.id = parseInt(person.url.split('/')[5]);
          this.specie.people_obj.push(person);
        });
      });
    });
  }

}
