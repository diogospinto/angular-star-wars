import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { Vehicle } from 'src/app/models/vehicle';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

  id:number;
  vehicle:Vehicle;

  constructor(private route:ActivatedRoute, private mainservice:MainserviceService) { }

  ngOnInit() {
    this.id=parseInt(this.route.snapshot.paramMap.get('id'));
    this.mainservice.getVehicle(this.id).subscribe(vehicle=>{
      this.vehicle = vehicle;
      this.vehicle.id = parseInt(this.vehicle.url.split('/')[5]);

      this.vehicle.films_obj = [];
      this.vehicle.pilots_obj = [];
      this.vehicle.films.forEach(filmUrl=>{
        this.mainservice.getMovie(parseInt(filmUrl.split('/')[5])).subscribe(film=>{
          film.id = parseInt(film.url.split('/')[5]);
          this.vehicle.films_obj.push(film);
        });
      });
      this.vehicle.pilots.forEach(pilotUrl=>{
        this.mainservice.getPerson(parseInt(pilotUrl.split('/')[5])).subscribe(pilot=>{
          pilot.id = parseInt(pilot.url.split('/')[5]);
          this.vehicle.pilots_obj.push(pilot);
        });
      });
    });
  }

}
