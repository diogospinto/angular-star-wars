import { Component, OnInit } from '@angular/core';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { Vehicle } from 'src/app/models/vehicle';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  vehicles: Vehicle[];
  res = [];

  constructor(private mainservice:MainserviceService) { }

  ngOnInit() {
    this.mainservice.getVehicles().subscribe(vehicles=>{
      this.res.push(vehicles);
      this.vehicles = this.res[0].results;

      this.vehicles.forEach(vehicle=>{
        vehicle.id = parseInt(vehicle.url.split('/')[5]);
        vehicle.films_obj = [];
        vehicle.pilots_obj = [];
        vehicle.films.forEach(filmUrl=>{
          this.mainservice.getMovie(parseInt(filmUrl.split('/')[5])).subscribe(film=>{
            film.id = parseInt(film.url.split('/')[5]);
            vehicle.films_obj.push(film);
          });
        });
        vehicle.pilots.forEach(pilotUrl=>{
          this.mainservice.getPerson(parseInt(pilotUrl.split('/')[5])).subscribe(pilot=>{
            pilot.id = parseInt(pilot.url.split('/')[5]);
            vehicle.pilots_obj.push(pilot);
          });
        });
      });
    });
  }

}
