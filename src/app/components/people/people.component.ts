import { Component, OnInit } from '@angular/core';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { People } from 'src/app/models/people';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {

  person:People;
  id:number;

  constructor(private mainservice: MainserviceService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.id=parseInt(this.route.snapshot.paramMap.get('id'));
    this.mainservice.getPerson(this.id).subscribe(people=>{

      this.person = people;

      this.person.id = parseInt(this.person.url.split('/')[5]);
      this.person.films_obj = [];
      this.person.films.forEach(filmlink=>{
        this.mainservice.getMovie(filmlink.split('/')[5]).subscribe(film=>{
          film.id = parseInt(film.url.split('/')[5]);
          this.person.films_obj.push(film);
        });
      });
    });
  }

}
