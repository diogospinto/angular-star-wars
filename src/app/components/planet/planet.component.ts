import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { Planet } from 'src/app/models/planet';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.css']
})
export class PlanetComponent implements OnInit {

  id:number;
  planet:Planet;

  constructor(private route:ActivatedRoute, private mainservice:MainserviceService) { }

  ngOnInit() {
    this.id=parseInt(this.route.snapshot.paramMap.get('id'));
    this.mainservice.getPlanet(this.id).subscribe(planet=>{
      this.planet = planet;
      this.planet.id = parseInt(this.planet.url.split('/')[5]);
      this.planet.people_obj = [];
      this.planet.films_obj = [];
      this.planet.residents.forEach(specieUrl=>{
        this.mainservice.getPerson(parseInt(specieUrl.split('/')[5])).subscribe(person =>{
          person.id = parseInt(person.url.split('/')[5]);
          this.planet.people_obj.push(person);
        });
      });
      this.planet.films.forEach(filmUrl=>{
        this.mainservice.getMovie(parseInt(filmUrl.split('/')[5])).subscribe(film=>{
          film.id = parseInt(film.url.split('/')[5]);
          this.planet.films_obj.push(film);
        });
      });
    });
  }

}
