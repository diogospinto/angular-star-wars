import { Component, OnInit, ElementRef, SecurityContext } from '@angular/core';
import { Film } from 'src/app/models/film';
import { Movie } from 'src/app/models/movie';
import { Video } from 'src/app/models/video';
import { MainserviceService } from 'src/app/services/mainservice.service';
import { MovieserviceService } from 'src/app/services/movieservice.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {

  film: Film;
  movie: Movie;
  video: Video;
  id: number;

  constructor(private mainservice:MainserviceService,private movieservice:MovieserviceService,private route: ActivatedRoute, private sanitization:DomSanitizer, private elementref:ElementRef) { }

  ngOnInit() {
    this.id=parseInt(this.route.snapshot.paramMap.get('id'));
    this.mainservice.getMovie(this.id).subscribe(f=>{
      this.film = f;
      this.movieservice.searchMovie(this.film.title).subscribe(m=>{
        this.movie = m.results[0];
        this.film.poster_path = this.movieservice.tmDBImgUrl+'/'+this.movie.poster_path;
        this.film.backdrop_path = this.movieservice.tmDBFullImgUrl+'/'+this.movie.backdrop_path;
        this.film.backdrop_path_safe = this.sanitization.bypassSecurityTrustStyle(`url(${this.film.backdrop_path})`);
        this.elementref.nativeElement.ownerDocument.body.style.backgroungImage = this.film.backdrop_path;

        this.movieservice.getVideos(this.movie.id).subscribe(v => {
          this.film.trailerUrl = `https://www.youtube.com/embed/${v.results[0].key}`;
        });
      });
    });
  }

}