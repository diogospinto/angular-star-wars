import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FilmsComponent } from './components/films/films.component';
import { PeoplesComponent } from './components/peoples/peoples.component';
import { PlanetsComponent } from './components/planets/planets.component';
import { SpeciesComponent } from './components/species/species.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { StarshipsComponent } from './components/starships/starships.component';
import { FilmComponent } from './components/film/film.component';
import { PeopleComponent } from './components/people/people.component';
import { PlanetComponent } from './components/planet/planet.component';
import { SpecieComponent } from './components/specie/specie.component';
import { VehicleComponent } from './components/vehicle/vehicle.component';
import { StarshipComponent } from './components/starship/starship.component';


const routes: Routes = [
  {
    path: '', redirectTo: '/films', pathMatch: 'full'
  },
  {
    path: 'films', component: FilmsComponent
  },
  {
    path:'films/:id',component: FilmComponent
  },
  {
    path: 'people', component: PeoplesComponent
  },
  {
    path: 'people/:id', component: PeopleComponent
  },
  {
    path: 'planets', component: PlanetsComponent
  },
  {
    path: 'planets/:id', component: PlanetComponent
  },
  {
    path: 'species', component: SpeciesComponent
  },
  {
    path: 'species/:id', component: SpecieComponent
  },
  {
    path: 'vehicles', component: VehiclesComponent
  },
  {
    path: 'vehicles/:id', component: VehicleComponent
  },
  {
    path: 'starships', component: StarshipsComponent
  },
  {
    path: 'starships/:id', component: StarshipComponent
  },
  {
    path: '**', redirectTo: '/films', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
