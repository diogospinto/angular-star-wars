import { SafeStyle } from '@angular/platform-browser';

export class Film {
    episode_id: number;
    title: string;
    opening_crawl: string;
    director: string;
    producer: string;
    release_date: Date;
    url: string;
    poster_path:string;
    backdrop_path:string;
    backdrop_path_safe: SafeStyle;
    id:number;
    trailerUrl: string;
}
