export class Movie {
    poster_path:string;
    backdrop_path:string;
    id:number;
    homepage:string;
    tagline:string;
    trailer: string;
}
