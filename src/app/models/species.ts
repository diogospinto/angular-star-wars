import { People } from './people';
import { Film } from './film';

export class Species {
    id:number;
    name:string;
    classification:string;
    designation:string;
    average_height:string;
    skin_colors:string;
    hair_colors:string;
    eye_colors:string;
    average_lifespan:string;
    homeworld:string;
    language:string;
    people:string[];
    films:string[];
    created:Date;
    edited:Date;
    url:string;
    people_obj:People[];
    films_obj:Film[];
}
