import { Film } from './film';
import { People } from './people';

export class Planet {
    id:number;
    name:string;
    rotation_period:string;
    orbital_period:string;
    diameter:string;
    climate:string;
    gravity:string;
    terrain:string;
    surface_water:string;
    population:string;
    residents:string[];
    films:string[];
    created:Date;
    edited:Date;
    url:string;
    people_obj:People[];
    films_obj:Film[];
}
