import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FilmsComponent } from './components/films/films.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FilmComponent } from './components/film/film.component';
import { Routes, RouterModule } from '@angular/router';
import { RomanNumeralPipe } from './custom-pipes/roman-numeral.pipe';
import { SafePipe } from './custom-pipes/safe.pipe';
import { PeoplesComponent } from './components/peoples/peoples.component';
import { PeopleComponent } from './components/people/people.component';
import { PlanetsComponent } from './components/planets/planets.component';
import { PlanetComponent } from './components/planet/planet.component';
import { SpeciesComponent } from './components/species/species.component';
import { SpecieComponent } from './components/specie/specie.component';
import { StarshipsComponent } from './components/starships/starships.component';
import { StarshipComponent } from './components/starship/starship.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { VehicleComponent } from './components/vehicle/vehicle.component';

// const routes : Routes = [{
//   path:'',component:FilmsComponent
// },
// {
//   path:'people', component:PeoplesComponent
// },{
//   path:'people/:id', component:PeopleComponent
// },{
//   path:'planet', component:PlanetsComponent
// },{
//   path:'planet/:id', component:PlanetComponent
// },{
//   path:'specie', component:SpeciesComponent
// },{
//   path:'specie/:id', component:SpecieComponent
// },{
//   path:'starship', component:StarshipsComponent
// },{
//   path:'starship/:id', component:StarshipComponent
// },{
//   path:'vehicle', component:VehiclesComponent
// },{
//   path:'vehicle/:id', component:VehicleComponent
// }]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FilmsComponent,
    FilmComponent,
    RomanNumeralPipe,
    SafePipe,
    PeoplesComponent,
    PeopleComponent,
    PlanetsComponent,
    PlanetComponent,
    SpeciesComponent,
    SpecieComponent,
    StarshipsComponent,
    StarshipComponent,
    VehiclesComponent,
    VehicleComponent
  ],
  imports: [
    // RouterModule.forRoot(routes),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  exports:[RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
